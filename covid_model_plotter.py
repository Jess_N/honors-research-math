import json
import string
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)





single_file_list = ['covid_quarantine_model.json', 'covid_social_distance_model.json', 'covid_gathering_cap_model.json', 
                    'covid_mask_model.json']

double_file_list = ['covid_quar_sd_model.json', 'covid_quar_cap.json', 'covid_quar_mask_model.json', 
                    'covid_sd_cap_model.json', 'covid_sd_mask_model.json', 'covid_mask_cap_model.json']

triple_file_list = ['covid_quar_sd_cap_model.json', 'covid_quar_sd_mask_model.json', 'covid_quar_mask_cap_model.json', 
                    'covid_sd_mask_cap_model.json']




f = open('covid_base_model.json')
data = json.load(f)
base_infection = data["big_data"][0]["average infection rate"]
base_fatality = data["big_data"][0]["average death rate"]
base_time = data["big_data"][0]["average time to herd immunity"]
f.close()

infections_single = [base_infection]
infections_double = [base_infection]
infections_triple = [base_infection]
infections_all = [base_infection]
fatalities_single = [base_fatality]
fatalities_double = [base_fatality]
fatalities_triple = [base_fatality]
fatalities_all = [base_fatality]
times_single = [base_time]
times_double = [base_time]
times_triple = [base_time]
times_all = [base_time]



for m in range(len(single_file_list)):
    f = open(single_file_list[m])
    data = json.load(f)

    infections_single.append(data["average infection rate"])
    infections_all.append(data["average infection rate"])
    fatalities_single.append(data["average death rate"])
    fatalities_all.append(data["average death rate"])
    times_single.append(data["average time to herd immunity"])
    times_all.append(data["average time to herd immunity"])

    f.close()


for m in range(len(double_file_list)):

    f = open(double_file_list[m])
    data = json.load(f)

    infections_double.append(data["average infection rate"])
    infections_all.append(data["average infection rate"])
    fatalities_double.append(data["average death rate"])
    fatalities_all.append(data["average death rate"])
    times_double.append(data["average time to herd immunity"])
    times_all.append(data["average time to herd immunity"])

    f.close()


for m in range(len(triple_file_list)):

    f = open(triple_file_list[m])
    data = json.load(f)

    infections_triple.append(data["average infection rate"])
    infections_all.append(data["average infection rate"])
    fatalities_triple.append(data["average death rate"])
    fatalities_all.append(data["average death rate"])
    times_triple.append(data["average time to herd immunity"])
    times_all.append(data["average time to herd immunity"])

    f.close()

f = open('covid_quar_sd_mask_cap_model.json')
data = json.load(f)
infections_all.append(data["average infection rate"])
fatalities_all.append(data["average death rate"])
times_all.append(data["average time to herd immunity"])
f.close()


singles = [infections_single, fatalities_single, times_single]
doubles = [infections_double, fatalities_double, times_double]
triples = [infections_triple, fatalities_triple, times_triple]
alls = [infections_all, fatalities_all, times_all]
colab = [singles, doubles, triples, alls]


single_titles = ['Covid Single Measure Infection Rates', 'Covid Single Measure Fatality Rates', 
                 'Covid Single Measure Times to Herd Immunity']
double_titles = ['Covid Double Measure Infection Rates', 'Covid Double Measure Fatality Rates', 
                 'Covid Double Measure Times to Herd Immunity']
triple_titles = ['Covid Triple Measure Infection Rates', 'Covid Triple Measure Fatality Rates', 
                 'Covid Triple Measure Times to Herd Immunity']
all_titles = ['Covid Infection Rates', 'Covid Fatality Rates', 'Covid Times to Herd Immunity']
colab_titles = [single_titles, double_titles, triple_titles, all_titles]


single_x_labels = ['Base', 'Q', 'S', 'C', 'M']
double_x_labels = ['Base', 'Q/S', 'Q/C', 'Q/M', 'S/C', 'S/M', 'C/M']
triple_x_labels = ['Base', 'Q/S/C', 'Q/S/M', 'Q/C/M', 'S/C/M']
all_x_labels = ['Base', 'Q', 'S', 'C', 'M', 'Q/S', 'Q/C', 'Q/M', 'S/C', 'S/M', 'C/M', 'Q/S/C', 
              'Q/S/M', 'Q/C/M', 'S/C/M', 'Q/S/C/M']
colab_x_labels = [single_x_labels, double_x_labels, triple_x_labels, all_x_labels]


single_names = ['Covid_Single_Measure_Avg_Infection_Rates.pdf', 'Covid_Single_Measure_Avg_Fatality_Rates.pdf', 
                'Covid_Single_Measure_Avg_Time_to_Herd_Immunity.pdf']
double_names = ['Covid_Double_Measure_Avg_Infection_Rates.pdf', 'Covid_Double_Measure_Avg_Fatality_Rates.pdf', 
                'Covid_Double_Measure_Avg_Time_to_Herd_Immunity.pdf']
triple_names = ['Covid_Triple_Measure_Avg_Infection_Rates.pdf', 'Covid_Triple_Measure_Avg_Fatality_Rates.pdf', 
                'Covid_Triple_Measure_Avg_Time_to_Herd_Immunity.pdf']
all_names = ['Covid_Avg_Infection_Rates.pdf', 'Covid_Avg_Fatality_Rates.pdf', 'Covid_Avg_Time_to_Herd_Immunity.pdf']
colab_names = [single_names, double_names, triple_names, all_names]

errors = [
    [ [], [], [] ],
    [ [], [], [] ],
    [ [], [], [] ],
    [ [], [], [] ]
]

f = open('Covid_CIs.json')
data = json.load(f)

for i in range(len(colab)):
    errors[i][0].append(data["bases"][0])
    errors[i][1].append(data["bases"][1])
    errors[i][2].append(data["bases"][2])

for i in range(len(colab[0][0])-1):
    errors[0][0].append(data["reg"][i][0])
    errors[0][1].append(data["reg"][i][1])
    errors[0][2].append(data["reg"][i][2])
    errors[3][0].append(data["reg"][i][0])
    errors[3][1].append(data["reg"][i][1])
    errors[3][2].append(data["reg"][i][2])

for i in range(len(colab[1][0])-1):
    errors[1][0].append(data["reg"][i + len(colab[0])][0])
    errors[1][1].append(data["reg"][i + len(colab[0])][1])
    errors[1][2].append(data["reg"][i + len(colab[0])][2])
    errors[3][0].append(data["reg"][i + len(colab[0])][0])
    errors[3][1].append(data["reg"][i + len(colab[0])][1])
    errors[3][2].append(data["reg"][i + len(colab[0])][2])

for i in range(len(colab[2][0])-1):
    errors[2][0].append(data["reg"][i + len(colab[0]) + len(colab[1])][0])
    errors[2][1].append(data["reg"][i + len(colab[0]) + len(colab[1])][1])
    errors[2][2].append(data["reg"][i + len(colab[0]) + len(colab[1])][2]) 
    errors[3][0].append(data["reg"][i + len(colab[0]) + len(colab[1])][0])
    errors[3][1].append(data["reg"][i + len(colab[0]) + len(colab[1])][1])
    errors[3][2].append(data["reg"][i + len(colab[0]) + len(colab[1])][2]) 

errors[3][0].append(data["reg"][-1][0])
errors[3][1].append(data["reg"][-1][1])
errors[3][2].append(data["reg"][-1][2])
    
f.close()


for i in range(len(colab)):
    for j in range(len(colab[i])):
        fig,axs = plt.subplots()
        axs.set_title(colab_titles[i][j])
        if j == 2:
            axs.set_ylabel("Time (days)")
        if i == 3 and j ==  0:
            axs.set_yscale("log")
        plt.errorbar(colab_x_labels[i], colab[i][j], errors[i][j], ecolor='lightgray', ls='none', marker='o')
        plt.xticks(rotation = 30)
        fig.savefig(colab_names[i][j], filetype = '.pdf')



