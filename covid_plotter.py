import json
import string
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)





file_list = ['covid_base_model_btwn.json', 'covid_base_model_clns.json', 'covid_base_model_gn.json']

base_infection = 0
base_fatality = 0
base_time = 0

f = open('covid_base_model.json')
data = json.load(f)
base_infection += data["big_data"][0]["average infection rate"]
base_fatality += data["big_data"][0]["average death rate"]
base_time += data["big_data"][0]["average time to herd immunity"]
f.close()




for m in range(len(file_list)):
    f = open(file_list[m])
    data = json.load(f)

    k = data["big_data"][0]["run type"]
    kind = k.replace("_", " ")

    set_points = ["Base"]
    infection_rates = [base_infection]
    fatality_rates = [base_fatality]
    times = [base_time]

    for i in data["big_data"]:
        set_points.append(str(i["value"]))
    for i in data["big_data"]:
        infection_rates.append(i["average infection rate"])
    for i in data["big_data"]:
        fatality_rates.append(i["average death rate"])
    for i in data["big_data"]:
        times.append(i["average time to herd immunity"])

    f.close()


    errors = [ [], [], [] ]
    f = open('Covid_CIs.json')
    data = json.load(f)

    errors[0].append(data["bases"][0])
    errors[1].append(data["bases"][1])
    errors[2].append(data["bases"][2])
    for i in range(len(infection_rates)-1):
        errors[0].append(data["gt"][m][i][0])
        errors[1].append(data["gt"][m][i][1])
        errors[2].append(data["gt"][m][i][2])

    f.close()


    plot_types = ["Avg Infection Rates", "Avg Fatality Rates", "Avg Time to Herd Immunity"]
    file_types = ["Avg_Infection_Rates", "Avg_Fatality_Rates", "Avg_Time_to_Herd_Immunity"]
    title_stem = "Covid " + kind + ":"
    titles = []
    for i in plot_types:
        titles.append(title_stem + " " + i)
    file_stem = "Covid_" + k + " "
    file_names = []
    for i in plot_types:
        file_names.append(file_stem + i + ".pdf")

    to_plot = [infection_rates, fatality_rates, times]
    if m == 2:
        x_axis = "# of Community Pods"
    else:
        x_axis = "# of Isolated Nodes"
    

    for i in range(len(titles)):
        fig,axs = plt.subplots()
        axs.set_title(titles[i])
        axs.set_xlabel(x_axis)
        plt.errorbar(set_points, to_plot[i], errors[i], ecolor = 'lightgray', ls = 'none', marker = 'o')
        fig.savefig(file_names[i], filetype = '.pdf')

