import json
import string
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
import scipy.stats as stats




def find_err(data, percent, confidence = 0.95):
    mean, var, std = stats.bayes_mvs(data)
    m = mean[0]
    ci_max = mean[1][1]
    err = ci_max - m
    if percent == True:
        err /= 100
    return err


covid_files = ['covid_quarantine_model.json', 'covid_social_distance_model.json', 
               'covid_gathering_cap_model.json', 'covid_mask_model.json', 'covid_quar_sd_model.json', 
               'covid_quar_cap.json', 'covid_quar_mask_model.json', 'covid_sd_cap_model.json', 
               'covid_sd_mask_model.json', 'covid_mask_cap_model.json', 'covid_quar_sd_cap_model.json', 
               'covid_quar_sd_mask_model.json', 'covid_quar_mask_cap_model.json', 'covid_sd_mask_cap_model.json',
               'covid_quar_sd_mask_cap_model.json']
covid_base_files = ['covid_base_model.json', 'covid_base_model_btwn.json', 'covid_base_model_clns.json',
                    'covid_base_model_gn.json']


reg_errors = np.empty([len(covid_files), 3])
base_errors = np.empty([3])
gt_errors = np.empty([len(covid_base_files)-1, 8, 3])


for m in range(len(covid_files)):
    f = open(covid_files[m])
    data = json.load(f)

    infections = []
    fatalities = []
    times = []

    for i in data["data by run"]:
        infections.append(i["infected count"])
        fatalities.append(i["death count"])
        times.append(i["time to herd immunity"])
    
    reg_errors[m][0] = find_err(infections, percent = True)
    reg_errors[m][1] = find_err(fatalities, percent = True)
    reg_errors[m][2] = find_err(times, percent = False)

    f.close()



f = open(covid_base_files[0])
data = json.load(f)

infections = []
fatalities = []
times = []

for i in data["big_data"]:
    for j in i["data by run"]:
        infections.append(j["infected count"])
        fatalities.append(j["death count"])
        times.append(j["time to herd immunity"])
        
base_errors[0] = find_err(infections, percent = True)
base_errors[1] = find_err(fatalities, percent = True)
base_errors[2] = find_err(times, percent = False)
    
f.close()


for m in range(len(covid_base_files)-1):
    f = open(covid_base_files[m])
    data = json.load(f)

    for i in range(len(data["big_data"])):
        infections = []
        fatalities = []
        times = []

        for j in data["big_data"][i]["data by run"]:
            infections.append(j["infected count"])
            fatalities.append(j["death count"])
            times.append(j["time to herd immunity"])
        
        gt_errors[m][i][0] = find_err(infections, percent = True)
        gt_errors[m][i][1] = find_err(fatalities, percent = True)
        gt_errors[m][i][2] = find_err(times, percent = False)



data = {"reg": reg_errors.tolist(), "bases": base_errors.tolist(), "gt": gt_errors.tolist()}
with open('Covid_CIs.json', 'w') as fp:
  json.dump(data, fp)




