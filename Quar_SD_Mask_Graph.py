import networkx as nx
from networkx.algorithms import community
import matplotlib.pyplot as plt
import numpy as np
import math
import json
import itertools



#General tracking
run_count = 0
runs = 1000
avg_infected = 0
avg_recovered = 0
avg_dead = 0
avg_time_to_herd_immunity = 0
data_list = []
mask_cap = 50

while run_count < runs:
    #Sets up initial connected random graph with single infected red node
    nodes= 100
    selected_nodes = []
    G = nx.connected_watts_strogatz_graph(n = nodes, k = 6, p = 1, tries = 100, seed = None) 
    G = nx.convert_node_labels_to_integers(G, first_label = 0)
    for i in range(nodes):
        G.add_node
        G.add_edge(i,i + nodes)
    for node in G:
        if node >= nodes:
            selected_nodes.append(node)


    color_map = []
    num_blue = nodes
    num_green = 0
    num_red = 1
    num_orange = 0
    num_black = 0
    num_infected_tot = 1

    initial_infected = 15 + nodes
    for node in G:
        if node in selected_nodes:
            if node == initial_infected:
                color_map.append("red")
            else: 
                color_map.append("blue")
        elif node == initial_infected - nodes:
            color_map.append("magenta") 
        else:
            color_map.append("white")   
    # nx.draw_shell(G, node_color=color_map, with_labels=True)
    # plt.show()

    infection_rate = 0.075
    t = 0
    end_time = 20
    infection_time_end = np.zeros(len(selected_nodes))
    infection_time_end[initial_infected - nodes] = 2
    quarantine_time = np.zeros(len(selected_nodes))
    quarantine_time[initial_infected - nodes] = 1    #initial case takes twice time to quarantine
    recovery_time_end = np.zeros(len(selected_nodes))
    removed_edges = np.empty([len(selected_nodes), 2])

    #Progression loop
    while t < end_time:
        if "red" not in color_map and "orange" not in color_map:
            break
        for node in G:
            node = int(node)
            #finds infected nodes
            if color_map[node] == "magenta":
                if color_map[node - nodes] != "orange":
                    cap = infection_rate * 100
                else:
                    cap = math.ceil(infection_rate * 75)
                for neighbor in nx.all_neighbors(G, node):
                    neighbor = int(neighbor)
                    if color_map[neighbor] == "white":
                        chance_mask_transmit = np.random.randint(1,100)
                        #if node is newly infected, determine symptom status
                        if chance_mask_transmit <= cap:
                            chance_infection = np.random.randint(1,100)
                            if chance_infection <= cap:
                                color_map[neighbor] = "magenta"
                                num_blue -= 1
                                infection_time_end[neighbor] = t + 2
                                chance_symptomatic = np.random.randint(1,100)
                                if chance_symptomatic > 40:
                                    color_map[neighbor + nodes] = "red"
                                    num_red += 1
                                    num_infected_tot += 1
                                    quarantine_time[neighbor] = t + 1
                                else:
                                    color_map[neighbor + nodes] = "orange"
                                    num_orange += 1
                                    num_infected_tot += 1
                                    quarantine_time[neighbor] = t + 2
                fatality_rate = np.random.randint(1,100000)
                #kill infected nodes by fatality rate, remove dead node edges
                if fatality_rate <= 702:
                    for neighbor in nx.all_neighbors(G, node):
                        if color_map[neighbor] == "orange":
                            num_orange -= 1
                            color_map[neighbor] = "black"
                            num_black += 1
                        elif color_map[neighbor] == "red":
                            num_red -= 1
                            color_map[neighbor] = "black"
                            num_black += 1
                    for ed in list(G.edges(node)):
                        G.remove_edge(ed[0], ed[1])
                    color_map[node] = "white"
                #quarantines symptomatic infected
                elif color_map[node - nodes] == "red" and quarantine_time[node] == t:
                    color_map[node - nodes] = "yellow"
                    color_map[node] = "white"
                    for ed in list(G.edges(node)):
                        G.remove_edge(ed[0], ed[1])
                        removed_edges[node][0] = ed[0]
                        removed_edges[node][1] = ed[1]
                #moves sick to recovered if not dead
                elif infection_time_end[node] == t:
                    for neighbor in nx.all_neighbors(G, node):
                        if color_map[neighbor] == "orange":
                            num_orange -= 1
                            color_map[neighbor] = "green"
                            num_green += 1
                        elif color_map[neighbor] == "yellow":
                            num_red -= 1
                            color_map[neighbor] = "green"
                            num_green += 1
                    recovery_time_end[node] = t + 15
                    #temporarily removes recovered node edges, store in array
                    for ed in list(G.edges(node)):
                        G.remove_edge(ed[0], ed[1])
                        removed_edges[node][0] = ed[0]
                        removed_edges[node][1] = ed[1]
                    color_map[node] = "white"
            #returns removed edges to node once immunity ends
            if recovery_time_end[node - nodes] == t:
                if color_map[node - nodes] == "green":
                    G.add_edge(removed_edges[node][0], removed_edges[node][1])
                    color_map[neighbor] = "blue"
                    num_green -= 1
                    num_blue += 1    

        # nx.draw_shell(G, node_color=color_map, with_labels=True)
        # plt.show()
        t += 1


    avg_infected += (num_infected_tot / runs)
    avg_dead += (num_black / runs)
    avg_recovered += ((num_infected_tot - num_black) / runs)
    avg_time_to_herd_immunity += ((t * 6) / runs)
    if run_count % 50 == 0:
        print("Done with run:", run_count)
    run_count += 1
    data_list.append({"run": run_count, "nodes": nodes, "death count": num_black, "infected count": num_infected_tot, 
                      "recovered count": num_infected_tot - num_black, "time to herd immunity": t * 6, 
                      "death rate": num_black / num_infected_tot, "infection rate": num_infected_tot / nodes, 
                      "recovery rate": (num_infected_tot - num_black) / nodes})

print("Avg infection rate:", avg_infected / nodes)
print("Avg death rate:", avg_dead / avg_infected)
print("Avg recovery rate:", avg_recovered / avg_infected)
print("Avg time to herd immunity:", avg_time_to_herd_immunity, "days")

data = {"data by run": data_list, "average infection rate": avg_infected / nodes, "average death rate": avg_dead / avg_infected, 
        "average recovery rate": avg_recovered / avg_infected, "average time to herd immunity": avg_time_to_herd_immunity}
with open('covid_quar_sd_mask_model.json', 'w') as fp:
  json.dump(data, fp)
