import networkx as nx
from networkx.algorithms import community
import matplotlib.pyplot as plt
import numpy as np
import math
import json
import itertools



def argsort(seq):
    return sorted(range(len(seq)), key=seq.__getitem__)



test_btwn = False
test_clns = False
test_gn = True


#betweenness centrality: numbers of isolated nodes:
bcn = [3, 5, 8, 10, 15, 20, 25, 33]
ccn = [3, 5, 8, 10, 15, 20, 25, 33]
gnn = [3, 4, 5, 8, 10, 13, 16, 20]

if test_btwn == True:
    choice = bcn
    run_type = "Betweenness_Centrality"
elif test_clns == True:
    choice = ccn
    run_type = "Closeness_Centrality"
elif test_gn == True:
    choice = gnn
    run_type = "Girvan_Newman"
else:
    choice = [0]
    run_type = "Base"


big_data_list = []

for point in range(len(choice)):
    #General tracking
    run_count = 0
    runs = 1000
    avg_infected = 0
    avg_recovered = 0
    avg_dead = 0
    avg_time_to_herd_immunity = 0
    data_list = []



    while run_count < runs:
        #Sets up initial connected random graph with single infected red node
        nodes = 100
        G = nx.connected_watts_strogatz_graph(n = nodes, k = 6, p = 1, tries = 100, seed = None) 
        G = nx.convert_node_labels_to_integers(G, first_label = 0)




        #Finds betweenness centrality for graph at start
        if test_btwn == True:
            btwns_cent = nx.betweenness_centrality(G, k=None, normalized=True, weight=None, endpoints=False, seed=None)
            betweenness = 0
            btwn_tracker = []
            btwn_node_tracker = []
            for key in btwns_cent:
                betweenness += btwns_cent[key]
                btwn_tracker.append(btwns_cent[key])
                btwn_node_tracker.append(key)
        
            order1 = np.argsort(btwn_tracker)
            sort_btwn = np.array(btwn_tracker)[order1]
            sort_btwn_node = np.array(btwn_node_tracker)[order1]
            high_btwn = sort_btwn[-choice[point]:]
            high_btwn_nodes = sort_btwn_node[-choice[point]:]


        #Finds closeness centrality for graph at start
        if test_clns == True:
            clns_cent = nx.closeness_centrality(G, u=None, distance=None, wf_improved=True)
            closeness = 0
            clns_tracker = []
            clns_node_tracker = []
            for key in clns_cent:
                closeness += clns_cent[key]
                clns_tracker.append(clns_cent[key])
                clns_node_tracker.append(key)

            order2 = np.argsort(clns_tracker)
            sort_clns = np.array(clns_tracker)[order2]
            sort_clns_node = np.array(clns_node_tracker)[order2]
            high_clns = sort_clns[-choice[point]:]
            high_clns_nodes = sort_clns_node[-choice[point]:]
            # high_clns_nodes = sort_clns_node[-10:]


        #Finds communities via Girvan Newman method
        if test_gn == True:
            k = choice[point]
            gn = nx.community.girvan_newman(G)
            limited = itertools.takewhile(lambda c: len(c) <= k, gn)
            comms = []
            for communities in limited:
                comms = list(sorted(c) for c in communities)
            


        


        color_map = []
        num_blue = nodes
        num_green = 0
        num_red = 1
        num_orange = 0
        num_black = 0
        num_infected_tot = 1

        initial_infected = 35


        for node in G:
            if node == initial_infected:
                color_map.append("red")
            elif test_btwn == True and node in high_btwn_nodes:
                color_map.append("gray")
            elif test_clns == True and node in high_clns_nodes:
                color_map.append("gray")
            else: 
                color_map.append("blue") 

        # nx.draw_shell(G, node_color=color_map, with_labels=True)
        # plt.show()

        #infection_rate = 0.05
        infection_rate = 0.15
        t = 0
        end_time = 20
        infection_time_end = np.zeros(nodes)
        infection_time_end[initial_infected] = 2
        recovery_time_end = np.zeros(nodes)
        removed_edges = np.empty([nodes, 2])





        while t < end_time:

            if "red" not in color_map and "orange" not in color_map:
                break

            dead_nodes = []
            immune_nodes = []
            returned_nodes = []

            for node in G:
                node = int(node)

                #finds infected nodes
                if color_map[node] == "red" or color_map[node] == "orange":
                    #determines which group the sick node is in
                    if test_gn == True:
                        for com in comms:
                            if node in com:
                                group = comms.index(com)

                    #sets new infection probability by symptom status     
                    if color_map[node] != "orange":
                        cap = infection_rate * 100
                    else:
                        cap = math.ceil(infection_rate * 75)

                    #finds all neighbors of infected node
                    for neighbor in nx.all_neighbors(G, node):
                        neighbor = int(neighbor)

                        same_group = False
                        if test_gn == True:
                            for com in comms:
                                if neighbor in com:
                                    group2 = comms.index(com)
                            if group == group2:
                                same_group = True

                        if color_map[neighbor] == "blue":
                            if test_gn == False or same_group == True:
                                chance_infected = np.random.randint(1,100)
                            else:
                                chance_infected = cap + 1

                            #if node is newly infected, determine symptom status
                            if chance_infected <= cap:
                                num_blue -= 1
                                infection_time_end[neighbor] = t + 2
                                chance_symptomatic = np.random.randint(1,100)
                                if chance_symptomatic > 40:
                                    color_map[neighbor] = "red"
                                    num_red += 1
                                    num_infected_tot += 1
                                else:
                                    color_map[neighbor] = "orange"
                                    num_orange += 1
                                    num_infected_tot += 1

                    #kill infected nodes by fatality rate, remove dead node edges
                    fatality_rate = np.random.randint(1,100000)
                    if fatality_rate <= 702:
                        if color_map[node] == "orange":
                            num_orange -= 1
                        elif color_map[node] == "red":
                            num_red -= 1
                        color_map[node] = "black"
                        num_black += 1
                        dead_nodes.append(node)
                        # for ed in list(G.edges(node)):
                        #     G.remove_edge(ed[0], ed[1])

                    #moves sick to recovered if not dead
                    elif infection_time_end[node] == t:
                        if color_map[node] == "orange":
                            num_orange -= 1
                        elif color_map[node] == "red":
                            num_red -= 1
                        color_map[node] = "green"
                        num_green += 1
                        recovery_time_end[node] = t + 15
                        immune_nodes.append(node)

                        #temporarily removes recovered node edges, store in array
                        # for ed in list(G.edges(node)):
                        #     G.remove_edge(ed[0], ed[1])
                        #     removed_edges[node][0] = ed[0]
                        #     removed_edges[node][1] = ed[1]

                #returns removed edges to node once immunity ends
                if color_map[node] == "green" and recovery_time_end[node] == t:
                    returned_nodes.append(node)
                    # G.add_edge(removed_edges[node][0], removed_edges[node][1])
                    color_map[node] = "blue"
                    num_green -= 1
                    num_blue += 1

        
            for i in range(len(dead_nodes)):
                for ed in list(G.edges(i)):
                    G.remove_edge(ed[0],ed[1])
            for i in range(len(immune_nodes)):
                for ed in list(G.edges(i)):
                    G.remove_edge(ed[0], ed[1])
                    removed_edges[immune_nodes[i]][0] = ed[0]
                    removed_edges[immune_nodes[i]][1] = ed[1]
            # for i in range(len(returned_nodes)):
                # G.add_edge(removed_edges[returned_nodes[i]][0], removed_edges[returned_nodes[i]][1])

            # nx.draw_shell(G, node_color=color_map, with_labels=True)
            # plt.show()
            t += 1




        avg_dead += (num_black / runs)
        avg_infected += (num_infected_tot /runs)
        avg_recovered += ((num_infected_tot - num_black) / runs)
        avg_time_to_herd_immunity += ((t * 6) / runs)
        if run_count % 50 == 0:
            print("Done with run:", run_count)
        run_count += 1
        data_list.append({"run": run_count, "nodes": nodes, "death count": num_black, "infected count": num_infected_tot, 
                        "recovered count": num_infected_tot - num_black, "time to herd immunity": t * 6, 
                        "death rate": num_black / num_infected_tot, "infection rate": num_infected_tot / nodes, 
                        "recovery rate": (num_infected_tot - num_black) / nodes})







    print("Avg infection rate:", avg_infected / nodes)
    print("Avg death rate:", avg_dead / avg_infected)
    print("Avg recovery rate:", avg_recovered / avg_infected)
    print("Avg time to herd immunity:", avg_time_to_herd_immunity, "days")

    data = {"run type": run_type, "value": choice[point], "data by run": data_list, "average infection rate": avg_infected / nodes, "average death rate": avg_dead / avg_infected, 
            "average recovery rate": avg_recovered / avg_infected, "average time to herd immunity": avg_time_to_herd_immunity}

    big_data_list.append(data)

big_data = {"big_data": big_data_list}

if test_btwn == True:
    name = 'covid_base_model_btwn.json'
elif test_clns == True:
    name = 'covid_base_model_clns.json'
elif test_gn == True:
    name = 'covid_base_model_gn.json'
else:
    name = 'covid_base_model.json'

with open(name, 'w') as fp:
    json.dump(big_data, fp)



